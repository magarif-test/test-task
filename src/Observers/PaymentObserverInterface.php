<?php

declare(strict_types=1);

namespace App\Observers;

use App\Events\PaymentSuccessEvent;

interface PaymentObserverInterface
{
    public function handle(PaymentSuccessEvent $event): void;
}
