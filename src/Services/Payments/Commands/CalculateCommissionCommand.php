<?php

declare(strict_types=1);

namespace App\Services\Payments\Commands;

use App\Entities\Fee;
use Money\Money;

final readonly class CalculateCommissionCommand
{
    public function __construct(
        public Money $amount,
        public Fee $fee,
    ) {
    }
}
