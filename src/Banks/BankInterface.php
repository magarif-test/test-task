<?php

declare(strict_types=1);

namespace App\Banks;

use App\Banks\Exceptions\BadPaymentMethodException;
use App\Banks\Exceptions\BankServerErrorException;
use App\Banks\Exceptions\BankConnectionTimeoutException;
use App\Banks\Responses\PaymentStatus;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

interface BankInterface
{
    /**
     * @throws BadPaymentMethodException
     * @throws BankServerErrorException
     * @throws BankConnectionTimeoutException
     */
    public function createPayment(Money $amount, PaymentMethodInterface $paymentMethod): PaymentStatus;
}
