<?php

declare(strict_types=1);

namespace App\Observers;

use App\Events\PaymentSuccessEvent;
use App\Output;
use App\PaymentMethods\Card;
use Money\Money;
use Override;

final readonly class CardEurNotificationObserver implements PaymentObserverInterface
{
    #[Override]
    public function handle(PaymentSuccessEvent $event): void
    {
        if (
            $event->payment->method instanceof Card
            && $event->payment->amount->isSameCurrency(Money::EUR(0))
            // && $event->payment->amount->getCurrency()->equals(new Currency('EUR'))
        ) {
            Output::info('Sending notification of a successful payment using a card in EUR currency');
        }
    }
}
