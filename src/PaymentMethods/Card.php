<?php

declare(strict_types=1);

namespace App\PaymentMethods;

use DateTimeImmutable;
use SensitiveParameter;

final readonly class Card implements PaymentMethodInterface
{
    public function __construct(
        public string $pan,
        public DateTimeImmutable $expiryDate,
        #[SensitiveParameter]
        public int $cvc,
    ) {
    }
}
