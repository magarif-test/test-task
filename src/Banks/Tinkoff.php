<?php

declare(strict_types=1);

namespace App\Banks;

use App\Banks\Exceptions\BadPaymentMethodException;
use App\Banks\Responses\PaymentStatus;
use App\PaymentMethods\PaymentMethodInterface;
use App\PaymentMethods\Qiwi;
use Money\Money;

final readonly class Tinkoff implements BankInterface
{

    /**
     * @inheritDoc
     */
    public function createPayment(Money $amount, PaymentMethodInterface $paymentMethod): PaymentStatus
    {
        if ($paymentMethod instanceof Qiwi) {
            throw new BadPaymentMethodException();
        }

        // throw new BankServerErrorException();
        // throw new BankConnectionTimeoutException();

        return PaymentStatus::Completed;
    }
}
