<?php

declare(strict_types=1);

namespace App\Services\Payments\Commands;

use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

final readonly class CreatePaymentCommand
{
    public function __construct(
        public Money $amount,
        public PaymentMethodInterface $paymentMethod,
        public Money $commission,
    ) {
    }
}
