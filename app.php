<?php

declare(strict_types=1);

use App\Banks;
use App\Exceptions\ApplicationException;
use App\Observers\CardEurNotificationObserver;
use App\Observers\QiwiNotificationObserver;
use App\Output;
use App\PaymentMethods;
use App\SuccessPaymentNotifier;
use App\Repositories\CommissionArrayRepository;
use App\Services\Payments\CalculateCommissionService;
use App\Services\Payments\ChargePaymentService;
use App\Services\Payments\Commands\CalculateCommissionCommand;
use App\Services\Payments\Commands\ChargePaymentCommand;
use App\Services\Payments\Commands\CreatePaymentCommand;
use App\Services\Payments\CreatePaymentService;
use Money\Money;

require_once './vendor/autoload.php';

try {
    $amount = Money::RUB(999_00);
    // $amount = Money::RUB(15_000_00);
    // $amount = Money::EUR(15_000_00);
    $method = new PaymentMethods\Card('4242424242424242', new DateTimeImmutable('2021-10-15'), 123);
    // $method = new PaymentMethods\Qiwi('+79876543210');
    $bank = new Banks\Sberbank();
    // $bank = new Banks\Tinkoff();

    $notifier = new SuccessPaymentNotifier();
    $notifier->addObserver(new CardEurNotificationObserver());
    $notifier->addObserver(new QiwiNotificationObserver());

    $commissionRepository = new CommissionArrayRepository(__DIR__ .'/resources/commissions.php');
    $fee = $commissionRepository->getFee($bank, $method, $amount);

    $calculateCommissionService = new CalculateCommissionService();
    $command = new CalculateCommissionCommand($amount, $fee);
    $commission = $calculateCommissionService->handle($command);

    $createPaymentService = new CreatePaymentService();
    $command = new CreatePaymentCommand($amount, $method, $commission);
    $payment = $createPaymentService->handle($command);

    $chargePaymentService = new ChargePaymentService($notifier);
    $command = new ChargePaymentCommand($payment, $bank);
    $response = $chargePaymentService->handle($command);

    Output::success($response->message());
} catch (ApplicationException $exception) {
    Output::error($exception->getMessage());
} catch (Throwable $exception) {
    Output::error('Payment error');
}
