<?php

declare(strict_types=1);

namespace App;

use App\Events\AbstractPaymentEvent;
use App\Events\PaymentSuccessEvent;
use App\Observers\PaymentObserverInterface;

final class SuccessPaymentNotifier
{
    /** @var PaymentObserverInterface[]  */
    private array $observers = [];

    public function addObserver(PaymentObserverInterface $observer): void
    {
        $this->observers[] = $observer;
    }

    public function notify(PaymentSuccessEvent $event): void
    {
        foreach ($this->observers as $observer) {
            $observer->handle($event);
        }
    }
}
