<?php

declare(strict_types=1);

namespace App\Services\Payments;

use App\Banks\Exceptions\BadPaymentMethodException;
use App\Banks\Exceptions\BankConnectionTimeoutException;
use App\Banks\Exceptions\BankServerErrorException;
use App\Banks\Responses\PaymentStatus;
use App\Events\PaymentSuccessEvent;
use App\SuccessPaymentNotifier;
use App\Services\Payments\Commands\ChargePaymentCommand;

final readonly class ChargePaymentService
{
    public function __construct(
        private SuccessPaymentNotifier $notifier,
    ) {
    }

    /**
     * @throws BadPaymentMethodException
     * @throws BankServerErrorException
     * @throws BankConnectionTimeoutException
     */
    public function handle(ChargePaymentCommand $command): PaymentStatus
    {
        $status = $command->bank->createPayment($command->payment->amount, $command->payment->method);

        if ($status->isCompleted()) {
            $this->notifier->notify(new PaymentSuccessEvent($command->payment));
        }

        return $status;
    }
}
