FROM php:8.3-cli-alpine3.19

ADD https://github.com/mlocati/docker-php-extension-installer/releases/latest/download/install-php-extensions /usr/local/bin/

RUN apk add --no-cache --update \
    bash \
    git \
  && chmod +x /usr/local/bin/install-php-extensions \
  && install-php-extensions  \
    @composer\
    bcmath \
    gmp \
    xdebug

RUN addgroup -g 1000 app && adduser -u 1000 -G app -s /bin/sh -D app

WORKDIR /app

USER app
