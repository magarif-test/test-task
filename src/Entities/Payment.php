<?php

declare(strict_types=1);

namespace App\Entities;

use App\PaymentMethods\PaymentMethodInterface;
use DateTimeImmutable;
use Money\Money;

final readonly class Payment
{
    public DateTimeImmutable $createdAt;

    public function __construct(
        public Money $amount,
        public Money $commission,
        public PaymentMethodInterface $method,
    ) {
        $this->createdAt = new DateTimeImmutable();
    }

    public function getGrossAmount(): Money
    {
        return $this->amount->add($this->commission);
    }
}
