<?php

declare(strict_types=1);

namespace App\Observers;

use App\Events\PaymentSuccessEvent;
use App\Output;
use App\PaymentMethods\Qiwi;
use Override;

final readonly class QiwiNotificationObserver implements PaymentObserverInterface
{
    #[Override]
    public function handle(PaymentSuccessEvent $event): void
    {
        if ($event->payment->method instanceof Qiwi) {
            Output::info('Sending notification of a successful payment using a Qiwi');
        }
    }
}
