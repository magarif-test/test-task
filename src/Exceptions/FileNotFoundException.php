<?php

declare(strict_types=1);

namespace App\Exceptions;

use Throwable;

final class FileNotFoundException extends ApplicationException
{
    public function __construct(
        string $message = 'File not found',
        int $code = 0,
        Throwable|null $previous = null
    ) {
        parent::__construct($message, $code, $previous);
    }
}
