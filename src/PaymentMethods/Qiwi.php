<?php

declare(strict_types=1);

namespace App\PaymentMethods;

final readonly class Qiwi implements PaymentMethodInterface
{
    public function __construct(
        public string $phone,
    ) {
    }
}
