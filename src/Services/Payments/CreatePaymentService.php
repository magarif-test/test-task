<?php

declare(strict_types=1);

namespace App\Services\Payments;

use App\Entities\Payment;
use App\Services\Payments\Commands\CreatePaymentCommand;

final readonly class CreatePaymentService
{
    public function handle(CreatePaymentCommand $command): Payment
    {
        return new Payment($command->amount, $command->commission, $command->paymentMethod);
    }
}
