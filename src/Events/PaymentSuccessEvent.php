<?php

declare(strict_types=1);

namespace App\Events;

use App\Entities\Payment;

final readonly class PaymentSuccessEvent
{
    public function __construct(
        public Payment $payment,
    ) {
    }
}
