DC = docker compose
RUN = run --rm --interactive --tty -u "$$(id -u):$$(id -g)" -w /app
DC_RUN = ${DC} ${RUN}
D_RUN = docker ${RUN}

ALPINE = ${D_RUN} -v "$$(pwd):/app" alpine
CLI = ${DC_RUN} php-cli
COMPOSER = ${CLI} composer

APP = ${CLI} php app.php



init: docker-down-clear \
	docker-pull docker-build docker-up \
	install

up: docker-up
down: docker-down
restart: down up



docker-up:
	${DC} up -d
docker-down:
	${DC} down --remove-orphans
docker-down-clear:
	${DC} down -v --remove-orphans
docker-pull:
	${DC} pull
docker-build:
	${DC} build --pull



install:
	${COMPOSER} i -o
update:
	${COMPOSER} u -o
outdated:
	${COMPOSER} outdated --direct



cli:
	${CLI} bash
run:
	${APP}
