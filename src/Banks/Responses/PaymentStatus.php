<?php

declare(strict_types=1);

namespace App\Banks\Responses;

enum PaymentStatus: int
{
    case Failed = 1;
    case Completed = 2;

    public function isFailed(): bool
    {
        return $this === self::Failed;
    }

    public function isCompleted(): bool
    {
        return $this === self::Completed;
    }

    public function message(): string
    {
        return match ($this) {
            self::Failed => 'Something went wrong! Try another payment method',
            self::Completed => 'Thank you! Payment completed',
        };
    }
}
