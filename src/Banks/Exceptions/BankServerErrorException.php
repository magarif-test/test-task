<?php

declare(strict_types=1);

namespace App\Banks\Exceptions;

use App\Exceptions\ApplicationException;
use Throwable;

final class BankServerErrorException extends ApplicationException
{
    public function __construct(
        string $message = 'Bank internal error',
        int $code = 0,
        Throwable $previous = null,
    ) {
        parent::__construct($message, $code, $previous);
    }
}
