<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Banks\BankInterface;
use App\Entities\Fee;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

interface CommissionRepositoryInterface
{
    public function getFee(
        BankInterface $bank,
        PaymentMethodInterface $paymentMethod,
        Money $amount,
    ): Fee;
}
