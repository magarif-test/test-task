<?php

declare(strict_types=1);

namespace App\Banks;

use App\Banks\Responses\PaymentStatus;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

final readonly class Sberbank implements BankInterface
{
    /**
     * @inheritDoc
     */
    public function createPayment(Money $amount, PaymentMethodInterface $paymentMethod): PaymentStatus
    {
        // throw new BankServerErrorException();
        // throw new BankConnectionTimeoutException();

        return PaymentStatus::Completed;
    }
}
