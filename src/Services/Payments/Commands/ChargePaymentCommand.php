<?php

declare(strict_types=1);

namespace App\Services\Payments\Commands;

use App\Banks\BankInterface;
use App\Entities\Payment;

final readonly class ChargePaymentCommand
{
    public function __construct(
        public Payment $payment,
        public BankInterface $bank,
    ) {
    }
}
