<?php

declare(strict_types=1);

use App\Banks\Sberbank;
use App\Banks\Tinkoff;
use App\PaymentMethods\Card;
use App\PaymentMethods\Qiwi;
use Money\Money;

return [
    Sberbank::class => [
        Card::class => [
            'RUB' => [
                [
                    'from' => Money::RUB(1_00),
                    'to' => Money::RUB(1_000_00),
                    'fee_percent' => 4,
                    'fee_fix' => Money::RUB(1_00),
                    'fee_min' => Money::RUB(3_00),
                ],
                [
                    'from' => Money::RUB(1_000_00),
                    'to' => Money::RUB(10_000_00),
                    'fee_percent' => 3,
                    'fee_fix' => Money::RUB(1_00),
                    'fee_min' => Money::RUB(3_00),
                ],
            ],
            'EUR' => [
                [
                    'from' => Money::EUR(1_00),
                    'to' => Money::EUR(10_000_00),
                    'fee_percent' => 7,
                    'fee_fix' => Money::EUR(1_00),
                    'fee_min' => Money::EUR(4_00),
                ],
            ],
        ],
        Qiwi::class => [
            'RUB' => [
                [
                    'from' => Money::RUB(1_00),
                    'to' => Money::RUB(75_000_00),
                    'fee_percent' => 5,
                    'fee_fix' => Money::RUB(0),
                    'fee_min' => Money::RUB(3_00),
                ],
            ],
        ],
    ],
    Tinkoff::class => [
        Card::class => [
            'RUB' => [
                [
                    'from' => Money::RUB(15_000_00),
                    'to' => null,
                    'fee_percent' => 2.5,
                    'fee_fix' => Money::RUB(0),
                    'fee_min' => Money::RUB(0),
                ],
            ],
        ],
    ],
];
