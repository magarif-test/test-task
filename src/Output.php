<?php

declare(strict_types=1);

namespace App;

final readonly class Output
{
    public static function success(string $message): void
    {
        echo "\e[92m", $message, "\e[0m", PHP_EOL;
    }

    public static function info(string $message): void
    {
        echo "\e[34m", $message, "\e[0m", PHP_EOL;
    }

    public static function error(string $message): void
    {
        echo "\e[31m", $message, "\e[0m", PHP_EOL;
    }
}
