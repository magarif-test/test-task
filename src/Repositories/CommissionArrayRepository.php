<?php

declare(strict_types=1);

namespace App\Repositories;

use App\Banks\BankInterface;
use App\Entities\Fee;
use App\Exceptions\FileNotFoundException;
use App\PaymentMethods\PaymentMethodInterface;
use Money\Money;

final readonly class CommissionArrayRepository implements CommissionRepositoryInterface
{
    /**
     * @throws FileNotFoundException
     */
    public function __construct(
        private string $filename,
    ) {
        if (!file_exists($filename)) {
            throw new FileNotFoundException();
        }
    }

    public function getFee(
        BankInterface $bank,
        PaymentMethodInterface $paymentMethod,
        Money $amount,
    ): Fee {
        $commissions = include $this->filename;

        $currency = $amount->getCurrency();
        $rules = $commissions[$bank::class][$paymentMethod::class][$currency->getCode()] ?? [];
        foreach ($rules as $rule) {
            if (
                (!isset($rule['from']) || $amount->greaterThanOrEqual($rule['from']))
                && (!isset($rule['to']) || $amount->lessThan($rule['to']))
            ) {
                return new Fee(
                    $rule['fee_percent'],
                    $rule['fee_fix'],
                    $rule['fee_min'],
                );
            }
        }

        return new Fee(
            0,
            new Money(0, $currency),
            new Money(0, $currency),
        );
    }
}
