<?php

declare(strict_types=1);

namespace App\Entities;

use Money\Money;

final readonly class Fee
{
    public function __construct(
        public float $percent,
        public Money $fix,
        public Money $min,
    ) {
    }
}
