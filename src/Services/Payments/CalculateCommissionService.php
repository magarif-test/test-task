<?php

declare(strict_types=1);

namespace App\Services\Payments;

use App\Banks\BankInterface;
use App\Banks\Commissions\CommissionStrategyFactory;
use App\PaymentMethods\PaymentMethodInterface;
use App\Services\Payments\Commands\CalculateCommissionCommand;
use Money\Money;

final readonly class CalculateCommissionService
{
    public function handle(CalculateCommissionCommand $command): Money
    {
        $feePercent = $command->fee->percent / 100;
        $fee = $command->amount->multiply($feePercent)->add($command->fee->fix); // seller commission
        $fee = $fee->divide(1 - $feePercent); // buyer commission

        return Money::max($fee, $command->fee->min);
    }
}
