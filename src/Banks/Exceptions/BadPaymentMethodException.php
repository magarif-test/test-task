<?php

declare(strict_types=1);

namespace App\Banks\Exceptions;

use App\Exceptions\ApplicationException;
use Throwable;

final class BadPaymentMethodException extends ApplicationException
{
    public function __construct(
        string $message = 'Bad payment method',
        int $code = 0,
        Throwable|null $previous = null,
    ) {
        parent::__construct($message, $code, $previous);
    }
}
